package com.cisco.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cisco.model.PersonalData;
import com.cisco.uniqueaadhar.R;

public class CustomListAdapter extends ArrayAdapter<PersonalData> {
	int color = 0;
	int selected = 0;
	int layoutResourceId;
	public static View newView;
	List<PersonalData> data = null;
	private final LayoutInflater mInflater;
	Context c;
	Activity m;
	TextView remainingCount;
	private static final int FRAME_TIME_MS = 10;
	private static final String KEY = "i";
	private TextView animatedText;
	boolean isRunning = false;

	public CustomListAdapter(Activity activity, Context mainActivity,
			int layoutResourceId, List<PersonalData> data) {
		super(mainActivity, layoutResourceId, data);
		mInflater = (LayoutInflater) mainActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.data = data;
		this.c = mainActivity;
		this.m = activity;
	}

	public PersonalData getItem(int position) {
		return data.get(position);
	}

	public void setData(List<PersonalData> data2) {
		clear();
		if (data2 != null) {
			for (PersonalData item : data2) {
				add(item);
			}
		}
	}

	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		if (v == null)
			v = mInflater.inflate(R.layout.element_post, parent, false);
		newView = v;
		final PersonalData cli = getItem(position);
		
		RelativeLayout rel=(RelativeLayout) v.findViewById(R.id.layoutElement);
		rel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
			}
		});

		TextView loc = (TextView) v.findViewById(R.id.location);
		loc.setText(cli.getLocation());

		TextView totalQuantity = (TextView) v.findViewById(R.id.comment);
		totalQuantity.setText("Total Quantity" + cli.getRiceAmount()
				+ cli.getSugarAmount() + cli.getMustardAmount());

		TextView date = (TextView) v.findViewById(R.id.textView1);
		date.setText(cli.getDate() + "");

		TextView name = (TextView) v.findViewById(R.id.name);
		name.setText(cli.getAadharId());

		return v;
	}

}