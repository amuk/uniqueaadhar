package com.cisco.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.cisco.dao.Dao;
import com.cisco.uniqueaadhar.ViewAll;

public class GetAllDetailsTask extends AsyncTask<String, Integer, JSONObject>{

		private String requestType = null;
		private Context mContext = null;
		private ProgressDialog progressDialog;
		private Activity activityObj = null;
		private String type = null;

		public GetAllDetailsTask(Activity m, Context mContext) {
			this.activityObj = m;
			this.mContext = mContext;
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			JSONObject jo = null;
			type = params[0];
			

			jo = Dao.getData(mContext, requestType, type);

			return jo;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progressDialog = new ProgressDialog(activityObj);
			progressDialog.setMessage("Downloading and updating...");
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			if (progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();

			if (result != null) {
				try {
					ViewAll.updateData(result.getJSONArray("consumers"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Toast.makeText(mContext, "Data fetched Successfully",
						Toast.LENGTH_LONG).show();
			}

		}
	}
