package com.cisco.model;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.mongodb.DB;

@Path("/UAService")
public class UAServiceImpl {
	
	UAActionDAO uaactiondao;
	
	@GET	
	@Path("/getAllConsumers")
	@Produces("application/json")
	public String getConsumers() {		
		try {
			UAAction uaaction = new UAAction();
			DB UADatabase = uaaction.initialize();
			uaactiondao = new UAActionDAO(UADatabase);				
			return uaactiondao.getAllConsumers();			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	@GET	
	@Path("/getAConsumer")
	@Produces("application/json")
	public String getConsumer(@QueryParam("aadharId") String id) {		
		try {
			UAAction uaaction = new UAAction();
			DB UADatabase = uaaction.initialize();
			uaactiondao = new UAActionDAO(UADatabase);				
			return uaactiondao.getAConsumer(id);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}	
	
	@GET	
	@Path("/getAvlQty")
	@Produces("application/json")
	public String getAvailableQty(@QueryParam("aadharId") String id) {		
		try {
			UAAction uaaction = new UAAction();
			DB UADatabase = uaaction.initialize();
			uaactiondao = new UAActionDAO(UADatabase);				
			return uaactiondao.getAvlQty(id);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "error";
	}
	
	@POST
	@Path("/sell")
	@Produces("application/json")
	public void sellItems(String jsonQty) {		
		UAAction uaaction = new UAAction();
		DB UADatabase = uaaction.initialize();
		uaactiondao = new UAActionDAO(UADatabase);
		
		try {	
			uaactiondao.Sell(jsonQty);	
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}
