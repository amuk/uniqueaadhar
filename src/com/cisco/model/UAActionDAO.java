package com.cisco.model;

import java.text.SimpleDateFormat;

import org.mongodb.morphia.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class UAActionDAO {

	DBCollection consumerdataCollection;
	DBCollection avlQtyCollection;
	
	public UAActionDAO(final DB UADatabase) {
		consumerdataCollection = UADatabase.getCollection("consumerdata");
		avlQtyCollection = UADatabase.getCollection("avlquantity");
	}
	
	/*
	 * Get all consumers' information -- For the view
	 * ====================================================================
	 */
	public String getAllConsumers() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		JsonObject ret = new JsonObject();
		BasicDBObject whereQuery = new BasicDBObject();
		DBCursor cursor = consumerdataCollection.find(whereQuery);
		JsonArray conArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();
			String sdock = dock.toString();					
			JsonObject con = (JsonObject) parser.parse(sdock);
			conArray.add(con);
		}
		ret.add("consumers", conArray);
		System.out.println(ret.toString());
		return ret.toString();
	}
		
	/*
	 * Retrieve all info for a particular consumer -- I/p : AId
	 * ====================================================================
	 */
	public String getAConsumer(String aadharId) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("aadharId", aadharId);		
		
		JsonObject ret = new JsonObject();		
		DBCursor cursor = consumerdataCollection.find(whereQuery);
		JsonArray conArray = new JsonArray();
		JsonParser parser = new JsonParser();
		
		while (cursor.hasNext()) {
			DBObject dock = cursor.next();			
			String sdock = dock.toString();
			JsonObject con = (JsonObject) parser.parse(sdock);
			conArray.add(con);
		}
		ret.add("consumer", conArray);
		System.out.println(ret.toString());
		return ret.toString();
	}
	
	/*
	 * Retrieve available quantity for a particular consumer -- I/p : AId
	 * ====================================================================
	 */
	public String getAvlQty(String aadharId) {
		String sdock;

		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("aadharId", aadharId);		
		System.out.println(whereQuery);
		DBCursor cursor = avlQtyCollection.find(whereQuery);
		if(cursor.hasNext()){			
			sdock = cursor.next().toString();			
		}		
		else
			sdock = "{\"aadharId\":\"" + aadharId + "\", \"rice\": 0, \"Wheat\": 0, \"Salt\": 0, \"Sugar\": 0, \"KeroseneOil\": 0, \"Lentil\": 0,\"MustardOil\": 0}";
		
		return sdock;
	}
	
	/*
	 * Record the sale in the DB
	 * ====================================================================
	 */
	
	public void Sell(String jsonQty){		
		try{
			BasicDBObject document = (BasicDBObject) JSON.parse(jsonQty);
			modifyQuantity(jsonQty);
			consumerdataCollection.insert(document);
			System.out.println("Sale successful");
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	/*
	 * Modify quantity
	 * ====================================================================
	 */
	public void modifyQuantity(String jsonQty){		
		int avlQ, rqdQ;			
		
		BasicDBObject rqd = (BasicDBObject) JSON.parse(jsonQty);
		String aadharId = rqd.getString("aadharId");
		BasicDBObject whereQuery = new BasicDBObject();
		whereQuery.put("aadharId", aadharId);
		
		String avlQty = getAvlQty(aadharId);
		BasicDBObject avl = (BasicDBObject) JSON.parse(avlQty);
		
		avlQ = avl.getInt("rice");
		rqdQ = rqd.getInt("rice");
		rqd.remove("rice");
		rqd.put("rice", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("Wheat");
		rqdQ = rqd.getInt("Wheat");
		rqd.remove("Wheat");
		rqd.put("Wheat", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("Salt");
		rqdQ = rqd.getInt("Salt");
		rqd.remove("Salt");
		rqd.put("Salt", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("Sugar");
		rqdQ = rqd.getInt("Sugar");
		rqd.remove("Sugar");
		rqd.put("Sugar", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("KeroseneOil");
		rqdQ = rqd.getInt("KeroseneOil");
		rqd.remove("KeroseneOil");
		rqd.put("KeroseneOil", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("Lentil");
		rqdQ = rqd.getInt("Lentil");
		rqd.remove("Lentil");
		rqd.put("Lentil", (avlQ - rqdQ));		
		
		avlQ = avl.getInt("MustardOil");
		rqdQ = rqd.getInt("MustardOil");
		rqd.remove("MustardOil");
		rqd.put("MustardOil", (avlQ - rqdQ));		
		
		avlQtyCollection.update(whereQuery, rqd);
		
		System.out.println("Modified the available quantity");
	}	
	
}
