package com.cisco.model;

import java.net.UnknownHostException;
import com.mongodb.DB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class UAAction {
	
	public DB initialize() {

		final MongoClient mongoClient;
		final DB UADatabase;

		try {
			mongoClient = new MongoClient(new MongoClientURI(
					"mongodb://localhost"));
			UADatabase = mongoClient.getDB("aadhar");	
			
			System.out.println("Initialized");
			return UADatabase;
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		}
		return null;
	}
}