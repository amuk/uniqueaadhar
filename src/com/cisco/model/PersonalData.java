package com.cisco.model;

public class PersonalData {
private String name;
private String quantity;
private String date;
private String location;
private String contact;
private String aadharId;
public String getAadharId() {
	return aadharId;
}
public void setAadharId(String aadharId) {
	this.aadharId = aadharId;
}
public int getRiceAmount() {
	return riceAmount;
}
public void setRiceAmount(int riceAmount) {
	this.riceAmount = riceAmount;
}
public int getWheatAmount() {
	return wheatAmount;
}
public void setWheatAmount(int wheatAmount) {
	this.wheatAmount = wheatAmount;
}
public int getSugarAmount() {
	return sugarAmount;
}
public void setSugarAmount(int sugarAmount) {
	this.sugarAmount = sugarAmount;
}
public int getKeroseneAmount() {
	return keroseneAmount;
}
public void setKeroseneAmount(int keroseneAmount) {
	this.keroseneAmount = keroseneAmount;
}
public int getLentilAmount() {
	return lentilAmount;
}
public void setLentilAmount(int lentilAmount) {
	this.lentilAmount = lentilAmount;
}
public int getMustardAmount() {
	return mustardAmount;
}
public void setMustardAmount(int mustardAmount) {
	this.mustardAmount = mustardAmount;
}
private int riceAmount;
private int wheatAmount;
private int sugarAmount;
private int keroseneAmount;
private int lentilAmount;
private int mustardAmount;


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}
public String getContact() {
	return contact;
}
public void setContact(String contact) {
	this.contact = contact;
}

}
