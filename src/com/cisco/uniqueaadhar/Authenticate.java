package com.cisco.uniqueaadhar;

import com.parse.ParseInstallation;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Authenticate extends ActionBarActivity {
	   public int pass=0;
	   public long time;
		public static final String SMS_RECIPIENT_EXTRA = "com.cisco.uniqueaadhar.SMS_RECIPIENT";
		public static final String ACTION_SMS_SENT = "com.cisco.uniqueaadhar.SMS_SENT_ACTION";
		public static final String ACTION_SMS_DELIVERED = "com.cisco.uniqueaadhar.SMS_DELIVERED_ACTION";
		private static final String PREFERENCE_SMS_ACCEPTED = "sms.accepted";
		private static final String PREFERENCES_SMS = "sms";
		String msg;
		private static final String SUBSCRIBER_ID = "subscriberId";
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        
    	ParseInstallation installation = ParseInstallation
				.getCurrentInstallation();
		installation.put("user","kimam");
		installation.saveInBackground();
		
        Button b1=(Button) findViewById(R.id.btnProceedSummary);
        b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),SellPurchase.class);
				startActivity(i);
				
			}
		});
        
        Button b3=(Button) findViewById(R.id.aadhaarAPI);
        b3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),AadhaarAuthenticatorActivity.class);
				startActivity(i);
				
			}
		});
        
        
        
        
        Button b2=(Button) findViewById(R.id.btnProceedOTP);
        b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String recipient="8197276767";
				 SmsManager sms = SmsManager.getDefault();
					
				time=System.currentTimeMillis();
					
				 sms.sendTextMessage(recipient, null, "Your One Time Pass", PendingIntent.getBroadcast(
                        Authenticate.this, 0, new Intent(ACTION_SMS_SENT), 0), PendingIntent.getBroadcast(Authenticate.this, 0,
    				            new Intent(ACTION_SMS_DELIVERED), 0));
				
			}
		});
    
    
    registerReceiver(new BroadcastReceiver() {
    	@Override
        public void onReceive(Context context, Intent intent) {
            String message = null;
            boolean error = true;
            switch (getResultCode()) {
            case Activity.RESULT_OK:
                message = "Message sent!";
                Toast toast1 = Toast.makeText(Authenticate.this,
        			    "Sms sent successfully", Toast.LENGTH_SHORT);
        				toast1.setGravity(Gravity.CENTER, 0, 0);
        				toast1.show();
        			
                error = false;                   
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                message = "Error.";
                Toast toast2 = Toast.makeText(Authenticate.this,
        			    "Sms failed..error", Toast.LENGTH_SHORT);
        				toast2.setGravity(Gravity.CENTER, 0, 0);
        				toast2.show();
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                message = "Error: No service.";
                Toast toast3 = Toast.makeText(Authenticate.this,
        			    "No service", Toast.LENGTH_SHORT);
        				toast3.setGravity(Gravity.CENTER, 0, 0);
        				toast3.show();
        				
        				 final Thread mSplashThread;
        		     
        		        mSplashThread =  new Thread(){
        		            @Override
        		            public void run(){
        		                try {
        		                   synchronized(this){
        		                        // Wait given period of time  
        		                        wait(7000);
        		                        
        		                    }
        		                }
        		                catch(InterruptedException ex){                    
        		                }
        		                // Run next activity
        		               
        		                stop(); 
        		                
        		            }
        		        };
        		        mSplashThread.setDaemon(true);
        		        mSplashThread.start();            		        
   //     				takeaction(1);
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                message = "Error: Null PDU.";
                Toast toast4 = Toast.makeText(Authenticate.this,
        			    "Null pdu", Toast.LENGTH_SHORT);
        				toast4.setGravity(Gravity.CENTER, 0, 0);
        				toast4.show();
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                message = "Error: Radio off.";
                Toast toast5 = Toast.makeText(Authenticate.this,
        			    "Radio off", Toast.LENGTH_SHORT);
        				toast5.setGravity(Gravity.CENTER, 0, 0);
        				toast5.show();
                break;
            }
        }
    }, new IntentFilter(ACTION_SMS_SENT));
    registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "SMS delivered", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "SMS not delivered", 
                            Toast.LENGTH_SHORT).show();
                    break;                        
            }
        }
    }, new IntentFilter(ACTION_SMS_DELIVERED));   
}
}