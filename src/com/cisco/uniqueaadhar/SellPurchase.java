package com.cisco.uniqueaadhar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class SellPurchase extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sellpurchase);

        
        Button submit=(Button) findViewById(R.id.btnProceedSummary);
        submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Thank You, The data has been updated!!", Toast.LENGTH_LONG).show();
				Intent i=new Intent(getApplicationContext(),ViewAll.class);
				startActivity(i);
				
			}
		});
    }  
    }