package com.cisco.uniqueaadhar;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.cisco.adapters.CustomListAdapter;
import com.cisco.model.PersonalData;
import com.cisco.tasks.GetAllDetailsTask;

public class ViewAll extends Activity{
public static ArrayList<PersonalData> arrayPersonal=new ArrayList<PersonalData>();	
static Activity m;	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		m=this;
	       GetAllDetailsTask git=new GetAllDetailsTask(this,getApplicationContext());
	       git.execute("getAllConsumers");
	       
	       Button buysell=(Button) findViewById(R.id.buysell);
	       buysell.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),Authenticate.class);
				startActivity(i);
				
				
			}
		});
	        
		
	}

	public static void updateData(JSONArray jsonArray) throws JSONException {
		
		for(int i=0;i<jsonArray.length();i++){
		  PersonalData personal=new PersonalData();
		  personal.setContact("Contact");
		  Calendar cal=Calendar.getInstance();
		  
		  personal.setDate(cal.getTime()+"");
		  personal.setLocation("Bangalore");
		  personal.setName("Contact"+(i+1));
		  personal.setAadharId(jsonArray.getJSONObject(i).getString("aadharId"));
		  personal.setRiceAmount(jsonArray.getJSONObject(i).getInt("rice"));
		  personal.setWheatAmount(jsonArray.getJSONObject(i).getInt("Wheat"));
		  personal.setSugarAmount(jsonArray.getJSONObject(i).getInt("Sugar"));
		  personal.setKeroseneAmount(jsonArray.getJSONObject(i).getInt("KeroseneOil"));
		  personal.setMustardAmount(jsonArray.getJSONObject(i).getInt("Lentil"));
  
		  arrayPersonal.add(personal);
	
		}
		
		CustomListAdapter custom=new CustomListAdapter(m, m.getApplicationContext(), R.layout.element_post, arrayPersonal);
		ListView list=(ListView) m.findViewById(R.id.listData);
		list.setAdapter(custom);
		
		
		
	}

}
