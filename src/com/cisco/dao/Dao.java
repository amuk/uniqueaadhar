package com.cisco.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.JsonReader;
import android.util.Log;

import com.cisco.utils.MyHttpClient;

public class Dao {

	public static JSONObject getJsonObject(Context context, String requestType,
			String type) {

		String BaseUrl = "http://pastebin.com/download.php";

		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		int timeoutSocket = 30000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		// // Instantiate the custom HttpClient
		HttpClient client = new MyHttpClient(httpParameters, context);

		HttpGet httpget = new HttpGet(BaseUrl + requestType);
		Log.v("request", "request is" + BaseUrl + requestType);
		httpget.setHeader("Accept", "application/json");

		httpget.setHeader(HTTP.CONTENT_TYPE, "application/json");
		HttpResponse resp = null;
		try {
			resp = client.execute(httpget);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String response = null;
		// JSONArray jArr = null;
		JSONObject jObject = null;
		if (resp != null) {
			try {
				HttpEntity entity = resp.getEntity();
				response = EntityUtils.toString(entity);

				Log.v("response", response);

				jObject = new JSONObject(response);

			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {

			}
			return jObject;
		} else {
			return null;
		}
	}

	public static JSONObject getData(Context context, String requestType,
			String type) {
//		String baseUrl = "http://localhost:8080/UniqueAadhar/rest/UAService/";
//		JSONObject jObj = null;
//		try {
//			// Create a URL for the desired page
//			URL url = new URL(baseUrl + requestType);
//
//			// Read all the text returned by the server
//			BufferedReader in = new BufferedReader(new InputStreamReader(
//					url.openStream()));
//			String str;
//			while ((str = in.readLine()) != null) {
//				jObj = new JSONObject(str);
//			}
//			in.close();
//		} catch (MalformedURLException e) {
//		} catch (IOException e) {
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String jSONString="{\"consumers\": [{\"_id\": {\"$oid\": \"5572d2403709f6c21564058a\" },\"aadharId\": \"999999990019\",\"rice\": 5, \"Wheat\": 1,\"Salt\": 3,\"Sugar\": 0,\"KeroseneOil\": 0,\"Lentil\": 0, \"MustardOil\": 0},{\"_id\": {\"$oid\": \"5572e6be591fe4e8e472d897\" },\"aadharId\": \"999999990019\", \"rice\": 3,\"Wheat\": 3,\"Salt\": 1,\"Sugar\": 0,\"KeroseneOil\": 0,\"Lentil\": 0,\"MustardOil\": 0 }]}";
		JSONObject temp = null;
		try {
			temp = new JSONObject(jSONString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		return temp;
	}
}